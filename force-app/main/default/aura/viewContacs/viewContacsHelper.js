({
    getContacts : function(component) {
        var accountId = component.get("v.recordId");
        var action = component.get("c.getContacts");
        console.log(accountId);
        
        action.setParams({ 
            accountId : accountId
        });

        action.setCallback(this, function(response) {
            if(response.getState() === "SUCCESS") {
                var responseValue = response.getReturnValue();
                console.log(responseValue);
                component.set("v.contacts", responseValue);                
            } else {
                console.log("Error " + response.getError[0].message);
            }
        });
        $A.enqueueAction(action);
    },

    updateContacts: function(component) {
        var contactList = component.get("v.contacts");
        var action = component.get("c.updateContacts");
        console.log(contactList);
        action.setParams({
            contactsToUpdate: contactList
        });
        console.log(contactList);
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                  console.log("Result getting Contacts: " + state);
                this.getContacts(component);
            } else {
                console.log("Failed with state: " + state);
            }
            
        });
        $A.enqueueAction(action);

        component.set("v.isEditable", false);
    },
})