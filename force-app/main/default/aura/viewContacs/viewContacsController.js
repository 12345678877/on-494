({
    doInit : function(component, event, helper) {
        helper.getContacts(component);       
        
    },
     
    editAll: function (component, event, helper) {
        component.set("v.isEditable", true);
    },


    updateChanges: function (component, event, helper) {

        var allValid = component.find('inputForm').reduce(function (validSoFar, inputCmp) {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

        if (allValid) {
            helper.updateContacts(component);
            console.log('all valid is ok');
        } else {
        console.log('Update FAILED, field verification failed');
        }
    },
})