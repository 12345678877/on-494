public with sharing class ViewContactsApexController {

    @AuraEnabled    
    public static List<Contact> getContacts(String accountId) {
        List<Contact> contacts = [
            SELECT AccountId,FirstName,LastName,Title,Email
            FROM Contact
            WHERE AccountId = :accountId
            
        ];
        return contacts;
    }

    @AuraEnabled
    public static List<Contact> updateContacts(List<Contact> contactsToUpdate) {
       update contactsToUpdate;
       return contactsToUpdate;
    }
}
