@IsTest
public with sharing class viewContactControllerTest {    

	@testSetup
	static void setup(){
		Account acc = new Account();
		acc.Name = 'AccountName';
		insert acc;

		Contact contact = new Contact();
		contact.FirstName = 'FirstName';
		contact.LastName = 'LastName';
		contact.Title = 'Title';
		contact.Email = 'email@gmail.com';
		contact.Accountid = acc.Id;
		insert contact;
	}

	@isTest
	static void testGetContacts(){

		List<Contact> contact = [SELECT id, Accountid
		                         FROM Contact
		                         WHERE LastName = 'LastName'
		                         LIMIT 1];

		ID accId = contact[0].AccountId;

		Test.startTest();
		List<Contact> viewContactsResultGet = viewContactsApexController.getContacts(accId);
		Test.stopTest();

		system.assertEquals(1, viewContactsResultGet.size());
		system.assertEquals('FirstName', viewContactsResultGet[0].FirstName);
	}
 
	@isTest 
	static void testUpdateContacts(){ 

		List<Contact> fNamebefore = [SELECT FirstName
		                             FROM Contact
		                             WHERE Email = 'email@gmail.com'
		                             LIMIT 1];

		system.assertEquals('FirstName', fNamebefore[0].FirstName);

		fNamebefore[0].FirstName = 'newFirstName';
 
		List<Contact> fnameAfter = new List<Contact>();
		Test.startTest();viewContactsApexController.updateContacts(fNamebefore);
		Test.stopTest();

		system.assertEquals('newFirstName', fnameAfter[0].FirstName); 
	} 
}

